#!/usr/bin/env python2
import cv2, time, threading, os, time, urllib, select, random
import cv2.cv as cv
import numpy as np
from concurrent.futures import ThreadPoolExecutor
from geometry_msgs.msg import Pose2D, Point
from bh_vision.msg import Field as FieldMsg

FIELD_WIDTH = 3.8
FIELD_HEIGHT = 2.743
CIRCLE_DIAMETER = 0.508

MINIMUM_MOMENT_SIZE = 50
BALL_MOMENTS = [25, 50]

ROBOT_RED = (np.array([0, 0, 0], np.uint8), np.array([0, 0, 0], np.uint8))
ROBOT_ORANGE = (np.array([5, 70, 200], np.uint8), np.array([20, 255, 255], np.uint8))
ROBOT_CYAN = (np.array([90, 30, 200], np.uint8), np.array([100, 255, 255], np.uint8)) #crappy cyan: lower the saturation to like 0
ROBOT_PURPLE = (np.array([120, 15, 200], np.uint8), np.array([165, 255, 255], np.uint8))
ROBOT_GREEN = (np.array([60, 25, 180], np.uint8), np.array([77, 255, 255], np.uint8))

BALL_COLOR = (np.array([170, 40, 200], np.uint8), np.array([180, 200, 255], np.uint8))

ROBOT_COLORS = {'red': ROBOT_RED,
        'orange': ROBOT_ORANGE,
        'cyan': ROBOT_CYAN,
        'purple': ROBOT_PURPLE,
        'green': ROBOT_GREEN}

DISP_IMAGE = None

def get_robot_attr_colors():
    """
    Gets a set of tuples and field attr mappings
    (SOME_COLOR_TUPLE, 'hasHome1', 'home1')
    """
    return [(ROBOT_GREEN, 'hasHome1', 'home1'),
            (ROBOT_ORANGE, 'hasHome2', 'home2'),
            (ROBOT_CYAN, 'hasAway1', 'away1'),
            (ROBOT_PURPLE, 'hasAway2', 'away2')]

def rotate_2d(origin, point, r):
    dx = point[0] - origin[0]
    dy = point[1] - origin[1]
    ndx = (dx*np.cos(r)) - (dy*np.sin(r))
    ndy = (dx*np.sin(r)) + (dy*np.cos(r))
    return (origin[0] + ndx, origin[1] + ndy)

def lloyds_cluster_points(points, mu):
    clusters = {}
    for m in mu:
        clusters[m] = []
    for point in points:
        p = np.array((point[0], point[1]))
        mudist = [(i[0], np.linalg.norm(p-np.array(i[1])))\
                for i in enumerate(mu)]
        bestmu = mu[min(mudist, key=lambda t: t[1])[0]]
        clusters[bestmu].append(point)
    return clusters

def lloyds_compute_centers(clusters):
    """
    Recomputes centers, returning the centers sorted by number of points in them
    """
    weights = []
    for mu, points in clusters.iteritems():
        px = [p[0] for p in points]
        py = [p[1] for p in points]
        w = [p[2] for p in points]
        x = np.average(px, weights=w)
        y = np.average(py, weights=w)
        weights.append((x, y, sum(w)))
    s = [(m[0], m[1]) for m in sorted(weights, key=lambda w: w[2])]
    s.reverse()
    return s

def lloyds_has_converged(mu, mu_old):
    return set(tuple(a) for a in mu) == set(tuple(a) for a in mu_old)

def lloyds_cluster(points, k):
    """
    Performs Lloyd's algorithm for clustering in k clusters. Points should be
    supplied in the format tuple(x, y, weight).
    """
    mu_old = [(x, y) for x, y, w in random.sample(points, k)]
    mu = [(x, y) for x, y, w in random.sample(points, k)]
    while set(tuple(a) for a in mu) != set(tuple(a) for a in mu_old):
        mu_old = mu
        clusters = lloyds_cluster_points(points, mu)
        mu = list(lloyds_compute_centers(clusters))
    return mu

class Field(object):
    def __init__(self, executor):
        self.executor = executor
        self.associations = {}
        self.associate('home1', 'green')
        self.associate('home2', 'orange')
        self.associate('away1', 'cyan')
        self.associate('away2', 'purple')
    def associate(self, attr, color):
        self.associations[attr] = color
    def get_frame(self):
        raise NotImplementedError()
    def calibrate_field(self):
        """
        Performs field calibration to get the center, size, and angle
        """
        # take 10 samples to average
        centers = [self.capture_field_center(self.get_frame()[0]) for i in range(0, 10)]
        avg = np.mean([c for c in centers if c is not None], axis=0)
        if len(avg) == 0:
            return False
        self.center = (int(avg[0]), int(avg[1]))
        self.size = (int(avg[2]), int(avg[3]))
        self.angle = avg[4]
        return True
    @property
    def calibration(self):
        """
        Getter for the current calibration
        """
        return {'center': self.center, 'size': self.size, 'angle': self.angle}
    @calibration.setter
    def calibration(self, value):
        """
        Setter for calibration
        """
        self.center = value['center']
        self.size = value['size']
        self.angle = value['angle']

    def read(self):
        begin = time.time()
        im = self.derive_field_bounds()
        #cv2.imshow('frame', im)
        end = time.time()
        return [], end - begin
        frame, t = self.get_frame()
        # convert to HSV space
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        # the majority color should be the field color. We will use this to
        # derive the line colors which will give us the dimensions of the field.
        channels = cv2.split(hsv)
        # find one circle in the value (greyscale) channel)
        circles = cv2.HoughCircles(channels[2], cv.CV_HOUGH_GRADIENT, 16, 2048)
        circle = circles[0][0]
        x = circle[0]
        y = circle[1]
        r = circle[2]
        im1 = cv2.inRange(hsv[y-r:y+r,x-r:x+r], np.array([0, 0, 0], np.uint8),
                np.array([180, 10, 127], np.uint8))
        #im2 = cv2.inRange(hsv, np.array([160, 100, 100], np.uint8),
        #        np.array([180, 255, 255], np.uint8))
        # find one circle
        out = frame
        for c in circles[0]:
            cv2.circle(out, (c[0], c[1]), c[2], (255, 255, 0))
        #contours, h = cv2.findContours(out, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #cv2.drawContours(frame, contours, -1, (255, 255, 0), 3)
        #cv2.imshow('frame', im1)
        end = time.time()
        return [], end - begin # contours, end - begin

    def convert_px_to_m(self, point):
        """
        Performs a pixel to meter conversion
        """
        dx = point[0] - self.center[0]
        dy = point[1] - self.center[1]
        scaling_factor = FIELD_WIDTH / self.size[0]
        return (dx*scaling_factor, dy*scaling_factor)

    def convert_m_to_px(self, point):
        """
        Performs a meter to pixel conversion
        """
        scaling_factor = self.size[0] / FIELD_WIDTH
        dx = self.center[0] + point[0] * scaling_factor
        dy = self.center[1] + point[1] * scaling_factor
        return (int(dx), int(dy))

    def begin_frame_processing(self, frame):
        """
        Handles cropping the frame, returning the cropped version and the
        resulting adjustment. Also converts the frame to hsv
        """
        # gather variables
        x, y, w, h = self.center[0], self.center[1], self.size[0], self.size[1]
        fh, fw, nc = frame.shape
        # crop the image
        cy1 = np.max([y-h/2, 0])
        cy2 = np.min([y+h/2, fh])
        cx1 = np.max([x-w/2, 0])
        cx2 = np.min([x+w/2, fw])
        dy = cy1
        dx = cx1
        cropped = frame[cy1:cy2, cx1:cx2]
        # convert to hsv
        hsv = cv2.cvtColor(cropped, cv2.COLOR_BGR2HSV) # 5-8ms
        return hsv, (dx, dy)

    def capture_robot_position(self, hsv, colors, delta=None):
        """
        Captures the a position from a frame
        """
        global DISP_IMAGE
        dx = 0 if delta is None else delta[0]
        dy = 0 if delta is None else delta[1]
        # find orange splotches
        im1 = cv2.inRange(hsv, colors[0], colors[1]) # 4ms
        #DISP_IMAGE = im1.copy()
        contours, h = cv2.findContours(im1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # 1ms
        # detect robot position. We expect exactly 2 orange sploteches
        moments = [cv2.moments(c) for c in contours]
        points = [(int(M['m10']/M['m00']), int(M['m01']/M['m00']), M['m00']) for M in moments if M['m00'] > 0]
        # If there are exactly two moments greater than MINIMUM_MOMENT_SIZE then
        # simply use those moment points. Otherwise, use Lloyd's algorithm for
        # clustering
        large_points = [p for p in points if p[2] > MINIMUM_MOMENT_SIZE]
        if len(large_points) == 2:
            points = sorted(large_points, key=lambda t: t[2]) # sort by moments
            points = [(x, y) for x, y, w in points]
        elif len(points) > 2:
            points = lloyds_cluster(points, 2)
        else:
            return None
        if len(points) != 2:
            # this happens somtimes. There is a bug in lloyds...but this is
            # easier to do than actually fixing the problem.
            return None
        # convert to field coordinates from cropped
        front = np.array([points[0][0] + dx, points[0][1] + dy], np.int32)
        back = np.array([points[1][0] + dx, points[1][1] + dy], np.int32)
        point = front / 2 + back / 2
        angle = np.arctan2(front[1] - back[1], front[0] - back[0])
        return (point[0], point[1]), angle
        moments = [M for M in moments if M['m00'] > MINIMUM_MOMENT_SIZE] # minimum area
        if len(moments) == 2:
            # sort robot blobs
            points = [(int(M['m10']/M['m00']), int(M['m01']/M['m00']), M['m00']) for M in moments]
            points = sorted(points, key=lambda t: t[2]) # sort by moments
            # convert to field coordinates from cropped
            front = np.array([points[0][0] + dx, points[0][1] + dy], np.int32)
            back = np.array([points[1][0] + dx, points[1][1] + dy], np.int32)
            point = front / 2 + back / 2
            angle = np.arctan2(front[1] - back[1], front[0] - back[0])
            return (point[0], point[1]), angle
        else:
            #print colors, "unseen: ", len(moments), "moments"
            return None

    def capture_ball_position(self, hsv, delta=None):
        """
        Captures the ball position from a frame
        """
        global DISP_IMAGE
        dx = 0 if delta is None else delta[0]
        dy = 0 if delta is None else delta[1]
        # find the magenta
        im1 = cv2.inRange(hsv, BALL_COLOR[0], BALL_COLOR[1]) # 4ms
        #DISP_IMAGE = im1.copy()
        contours, h = cv2.findContours(im1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # 1ms
        moments = [cv2.moments(c) for c in contours]
        #print [M['m00'] for M in moments]
        moments = [(int(M['m10']/M['m00']), int(M['m01']/M['m00']), M['m00']) for M in moments if M['m00'] > 0]
        px = [m[0] for m in moments]
        py = [m[1] for m in moments]
        weights = [m[2] for m in moments]
        if len(moments) > 0:
            x = np.average(px, weights=weights)
            y = np.average(py, weights=weights)
            return (x + dx, y + dy)
        else:
            return None

    def capture_field_center(self, frame):
        """
        Captures the field dimensions from a frame
        """
        # convert to HSV space
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        channels = cv2.split(hsv)
        # find one circle in the value (greyscale) channel)
        # from this we can derive the size of our field. The circle has a
        # diameter that is 1/8 of the length of the field
        circles = cv2.HoughCircles(channels[2], cv.CV_HOUGH_GRADIENT, 16, 2048)
        circle = circles[0][0]
        x = circle[0]
        y = circle[1]
        r = circle[2]
        ir = r * 0.707 * 0.9 # this will be inside the circle, leaving a line.
        # determine the orientation of the field by looking for a vertical line
        # we can cycle through the values until we get an image with a single
        # line in it. Our region of interest is pre-blurred to remove noise.
        low = np.array([0], np.uint8)
        high = np.array([127], np.uint8)
        roi = cv2.blur(channels[2][y-ir:y+ir,x-ir:x+ir], (5, 5))
        for i in range(0, 50): # for prevents infinite loop
            # a good mean should be 20ish given the ratio of line tickness to the
            # size of the circle. We adjust our threshold until we see something
            # like that
            thres = cv2.inRange(roi, low, high)
            mean = cv2.mean(thres)[0]
            if mean > 25: # we see too much: lower the high
                high = np.array([high[0] - 1], np.uint8)
            elif mean < 18:
                high = np.array([high[0] + 1], np.uint8)
            else:
                break # we have a good mean
        # after making a good image, find the lines
        lines = cv2.HoughLinesP(thres, 1, np.pi/180, 20)
        if lines is None:
            print "No lines!"
            return None
        angles = [np.arctan2(y2 - y1, x2 - x1) for x1, y1, x2, y2 in lines[0]]
        angles = [a + np.pi if a < 0 else a for a in angles] # make all be in the same hemisphere
        angles = [a - np.pi/2 for a in angles] # rotate -90 since line is perpendicular to x
        field_angle = np.median(angles) # the median angle will be our angle?
        # TODO: Test using the median for this better...its not strong
        field_width = r * FIELD_WIDTH / CIRCLE_DIAMETER * 2
        field_height = r * FIELD_HEIGHT / CIRCLE_DIAMETER * 2
        return (x, y, field_width, field_height, field_angle)

    def draw(self):
        """
        Captures and draws the full field
        """
        frame, t = self.get_frame()
        field = self.get_field(frame=frame)
        if field.hasHome1:
            self.draw_robot(frame, field.home1, (0, 255, 127))
        if field.hasHome2:
            self.draw_robot(frame, field.home2, (255, 0, 255))
        if field.hasAway1:
            self.draw_robot(frame, field.away1, (255, 127, 0))
        if field.hasAway2:
            self.draw_robot(frame, field.away2, (127, 255, 0))
        if field.hasBall:
            point = self.convert_m_to_px((field.ball.x, field.ball.y))
            cv2.circle(frame, point, 3, (255, 0, 0), 2)
        x = self.center[0]
        y = self.center[1]
        field_width = self.size[0]
        field_height = self.size[1]
        field_angle = self.angle
        points = [(x-field_width/2, y-field_height/2),
                (x+field_width/2, y-field_height/2),
                (x+field_width/2, y+field_height/2),
                (x-field_width/2, y+field_height/2)]
        translated = [rotate_2d((x, y), p, field_angle) for p in points]
        translated = [(int(p[0]), int(p[1])) for p in translated]
        cv2.line(frame, translated[0], translated[1], (255, 255, 0), 3)
        cv2.line(frame, translated[1], translated[2], (255, 255, 0), 3)
        cv2.line(frame, translated[2], translated[3], (255, 255, 0), 3)
        cv2.line(frame, translated[3], translated[0], (255, 255, 0), 3)
        return frame, field.hasBall

    def draw_robot(self, frame, pose, color):
        point = self.convert_m_to_px((pose.x, pose.y))
        #print point, pose.theta
        robot = [(point[0], point[1]),
                (int(point[0] + 50 * np.cos(pose.theta)), int(point[1] + 50*np.sin(pose.theta)))]
        cv2.line(frame, robot[0], robot[1], color, 2)

    def get_field(self, frame=None):
        """
        Grabs a frame and processes the data into a Field message
        """
        then = time.time()
        if frame is None:
            frame, ts = self.get_frame()
        else:
            ts = then
            
        #frame = cv2.blur(frame, (5, 5))
        hsv, delta = self.begin_frame_processing(frame)
        ballrs = self.executor.submit(self.capture_ball_position, hsv, delta=delta)
        robotsrs = [(a, self.executor.submit(self.capture_robot_position, hsv, ROBOT_COLORS[name], delta=delta)) for a, name, in self.associations.items()]
        #ball = self.capture_ball_position(hsv, delta=delta)
        #robots = [(a, self.capture_robot_position(hsv, ROBOT_COLORS[name], delta=delta)) for a, name in self.associations.items()]
        ball = ballrs.result()
        robots = [(a, rs.result()) for a, rs in robotsrs]
        now = time.time()
        msg = FieldMsg()
        msg.timestamp = ts
        msg.processingTime = now - then
        for attr, robot in robots:
            if robot is not None:
                hasAttr = 'has' + attr.capitalize()
                setattr(msg, hasAttr, True)
                point = self.convert_px_to_m(robot[0])
                setattr(msg, attr, Pose2D(point[0], point[1], robot[1]))
        if ball is not None:
            point = self.convert_px_to_m(ball)
            msg.hasBall = True
            msg.ball = Point(point[0], point[1], 0)
        return msg

    def stop(self):
        pass

class VideoField(Field):
    def __init__(self, executor, url):
        super(VideoField, self).__init__(executor)
        self.url = url
        self.run = True
        self.frame_buf = None
        self.frame_time = None
        self.delay_flag = False
        self.cap_sem = threading.Semaphore(0)
        thread = threading.Thread(target=self.__update, args=())
        thread.daemon = True
        thread.start()
    def __update(self):
        then = time.time()
        stream = urllib.urlopen(self.url)
        buf = ''
        a = -1
        b = -1
        # mmkay how this works:
        #
        # The VideoCapture has all this internal buffer crap and eventually
        # introduces delays. There's also the problem of the network stream
        # buffering stuff while we process an image. The purpose of this little
        # blurb here is to manually process the image stream and offload the
        # task of decoding the image to another thread via the executor.
        #
        # The mjpeg format is basically just a stream of jpg's enclosed by
        # \xff\xd8 and \xff\xd9 byte sequences. So, if we manage to find these
        # two sequences, we have just found a jpg and we can send it off to
        # opencv to decode. Now, so that this decoding process does not stop us
        # from reading the stream (thus causing frames to back up in the
        # network buffer), we use our concurrent.futures executor to actually
        # do the image decoding. We save the latest frame future only which
        # also ensures that only the latest frame is used when get_frame is
        # called.
        while self.run:
            buf += stream.read(1024)
            if a == -1:
                a = buf.find('\xff\xd8')
            elif b == -1:
                b = buf.find('\xff\xd9', a)
            else: # a and b != -1, so we found a frame
                jpg = buf[a:b+2]
                buf = ''#buf[b+2:]
                a = -1
                b = -1
                self.frame_buf = jpg
                self.frame_time = time.time()
                self.cap_sem.release()
                stream.close()
                stream = urllib.urlopen(self.url)
    def stop(self):
        self.run = False
    def get_frame(self):
        with self.cap_sem:
            img = cv2.imdecode(np.fromstring(self.frame_buf, dtype=np.uint8), cv2.CV_LOAD_IMAGE_COLOR)
            return img, self.frame_time

class ImageField(Field):
    def __init__(self, executor, filename):
        super(ImageField, self).__init__(executor)
        self.image = np.asarray(cv2.cv.LoadImage(filename, cv2.IMREAD_COLOR)[:,:])
        self.buf = np.empty(self.image.shape, np.uint8)
    def get_frame(self):
        self.buf[:] = self.image[:]
        return self.buf, time.time()

curdir = os.path.dirname(os.path.realpath(__file__))

def main(executor):
    global DISP_IMAGE
    field = VideoField(executor, 'http://192.168.1.78:8080/stream?topic=/image&dummy=param.mjpg')
    #field = ImageField(executor, curdir + '/latest.png')
    field.calibrate_field()
    while True:
        then = time.time()
        DISP_IMAGE = None
        frame, yay = field.draw()
        if DISP_IMAGE is None:
            DISP_IMAGE = frame
        now = time.time()
        cv2.imshow('frame', DISP_IMAGE)
        k = cv2.waitKey(1) & 0xFF
        if k == ord('d'):
            print now - then
        if k == ord('e'):
            field.delay_flag = True
        if k == ord('q'):
            break
        if k == ord('s'):
            cv2.imwrite('latest.png', frame)
            print 'Image saved'
    field.stop()

if __name__ == '__main__':
    with ThreadPoolExecutor(max_workers=8) as e:
        main(e)

