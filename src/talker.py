#!/usr/bin/env python2

import os, json
import rospy
from geometry_msgs.msg import Pose2D
from bh_vision.msg import Field
from bh_vision.srv import CalibrateVision, CalibrateVisionResponse
from concurrent.futures import ThreadPoolExecutor
from threading import Lock
import field

CONFIG_FILE = 'config.json'

curdir = os.path.dirname(os.path.realpath(__file__))

appdata = os.path.expanduser(os.path.join('~', '.config', 'bh_vision'))

def ensure_appdata():
    if not os.path.exists(appdata):
        os.makedirs(appdata)

class CalibrationException(Exception):
    pass

class Config():
    def __init__(self):
        ensure_appdata()
        p = os.path.join(appdata, CONFIG_FILE)
        if os.path.isfile(p):
            with open(p, 'r') as f:
                data = f.read()
                self.__config = json.loads(data)
        else:
            self.__config = {}
    
    def __write_config(self):
        p = os.path.join(appdata, CONFIG_FILE)
        with open(p, 'w') as f:
            data = json.dumps(self.__config, indent=4)
            f.write(data)
    def config_calibration(self, url, calibration=None):
        if calibration is not None:
            if 'calibration' not in self.__config:
                self.__config['calibration'] = {}
            self.__config['calibration'][url] = calibration
            self.__write_config()
            return calibration
        if 'calibration' in self.__config and url in self.__config['calibration']:
            return self.__config['calibration'][url]
        else:
            return None

class Node():
    def __init__(self, executor, url):
        self.config = Config()
        self.field_lock = Lock()
        self.url = url
        self.field = field.VideoField(executor, self.url)
        #self.field = field.ImageField(executor, curdir + '/latest.png')
        calibration = self.config.config_calibration(self.url)
        if calibration is None:
            print "No calibration for this url. Calibrating field."
            if not self.field.calibrate_field():
                raise CalibrationException()
            self.config.config_calibration(self.url, self.field.calibration)
        else:
            print "Using existing calibration"
            self.field.calibration = calibration
        rospy.init_node('talker')
        # declare the possible colors
        rospy.set_param('colors', field.ROBOT_COLORS.keys())
        # declare the default associations
        rospy.set_param('associations', self.field.associations)
        pub = rospy.Publisher('positions', Field, queue_size=10)
        calibrate_service = rospy.Service('calibrate_vision', CalibrateVision,\
                self.request_recalibration)
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            with self.field_lock:
                # set the associations
                for k, v in rospy.get_param('associations').items():
                    self.field.associate(k, v)
                # get the field
                msg = self.field.get_field()
            if msg is not None:
                pub.publish(msg)
                r.sleep()
    def request_recalibration(self, data):
        with self.field_lock:
            print "Recalibrating field"
            if self.field.calibrate_field():
                self.config.config_calibration(self.url, self.field.calibration)
                print "success"
                return CalibrateVisionResponse("ok")
            else:
                print "failure"
                return CalibrateVisionResponse("error")

if __name__ == '__main__':
    with ThreadPoolExecutor(max_workers=8) as e:
        try:
            nd = Node(e, 'http://192.168.1.78:8080/stream?topic=/image&dummy=param.mjpg') # home
            #nd = Node(e, 'http://192.168.1.79:8080/stream?topic=/image&dummy=param.mjpg') # away
        except rospy.ROSInterruptException:
            pass

